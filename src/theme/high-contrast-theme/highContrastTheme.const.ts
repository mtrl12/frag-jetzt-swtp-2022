export const highcontrast = {

  '--primary': 'white',
  '--primary-variant': 'DarkSlateGray',

  '--secondary': 'white',
  '--secondary-variant': '#fb9a1c',

  '--background': '#121212',
  '--surface': '#1e1e1e',
  '--dialog': '#000000',
  '--cancel': 'red',
  '--alt-surface': '#323232',
  '--alt-dialog': '#455a64',

  '--on-primary': '#141414',
  '--on-secondary': '#141414',
  '--on-primary-variant': '#FFFFFF',
  '--on-background': '#FFFFFF',
  '--on-surface': '#FFFFFF',
  '--on-dialog': '#FFFFFF',
  '--on-cancel': 'black',

  '--green': 'green',
  '--red': 'red',
  '--white': '#ffffff',
  '--yellow': 'darkorange',
  '--blue': '#3833e9',
  '--purple': 'purple',
  '--magenta': '#ea0a8e',
  '--light-green': '#33e98d',
  '--grey': '#7e7e7e',
  '--grey-light': '#9c9c9c',
  '--black': 'black',
  '--moderator': 'black',

  '--questionwall-intro-primary':'darkorange',
  '--questionwall-intro-secondary':'#eadabf',
  '--questionwall-intro-background':'#121212'

};

export const highcontrast_meta = {

  translation: {
    name: {
      en: 'High contrast',
      de: 'Hoher Kontrast',
    },
  },
  isDark: true,
  availableOnMobile: true,
  order: 0,
  scale_desktop: 1,
  scale_mobile: 1,
  previewColor: 'secondary',
  icon: 'contrast'

};


















